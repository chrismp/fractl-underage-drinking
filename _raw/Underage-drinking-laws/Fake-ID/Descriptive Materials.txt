
   CHANGES TO THE DATA
   ===================

Data for this policy topic are updated from time to time to add new material and to clarify or correct information already available on the site. A list of any changes since this policy topic first appeared on this Web site is available on the Web Site Change Log page.



   EXPLANATORY NOTES AND LIMITATIONS
   =================================

Explanatory Notes and Limitations Applicable to All APIS Policy Topics

1.   State law may permit local jurisdictions to impose requirements in addition to those mandated by State law. Alternatively, State law may prohibit local legislation on this topic, thereby preempting local powers. For more information on the preemption doctrine, see the About Alcohol Policy page. APIS does not document policies established by local governments.

2.   In addition to statutes and regulations, judicial decisions (case law) also may affect alcohol-related policies. APIS does not review case law except to determine whether judicial decisions have invalidated statutes or regulations that would otherwise affect the data presented in the comparison tables.

3.   APIS reviews published administrative regulations. However, administrative decisions or directives that are not included in a State's published regulatory codes may have an impact on implementation. This possibility has not been addressed by the APIS research.

4.   Statutes and regulations cited in tables on this policy topic may have been amended or repealed after the specific date or time period specified by the site user's search criteria.

5.   If a conflict exists between a statute and a regulation addressing the same legal issue, APIS coding relies on the statute.

6.   A comprehensive understanding of the data presented in the comparison tables for this policy topic requires examination of the applicable Row Notes and Jurisdiction Notes, which can be accessed from the body of the table via links in the Jurisdiction column.


Explanatory Notes and Limitations Specifically Applicable to False Identification for Obtaining Alcohol 

1.    States are coded as prohibiting false identification when it is a criminal offense to make a false statement regarding one's age when attempting to purchase alcohol, even if regulations and statutes do not specifically mention false identification.  This coding is justified because using a false ID is subsumed under the violation of "making a false statement regarding one's age."

2.    This review does not address the following issues related to False Identification: 

.    Provisions that impose license suspension only for second and subsequent offenses, or only for failure to successfully complete a diversion program, or only as a condition of probation. 
.   Provisions that prohibit making a false statement to a State's licensing authority for the purpose of obtaining a driver's license or other identification document. 
.   Provisions that provide an affirmative defense to the retailer only in civil liability (dram shop) causes of action, where a third party sues the retailer for damages that resulted from the retailer providing alcohol to a minor. 
.   Provisions that would grant immunity to a retailer for notifying authorities of a violation or suspected violation relating to underage purchase or use of false ID. 
.   Provisions that authorize license sanctions for alcohol-related offenses other than possession, consumption, or purchase, e.g., "intoxication," "drunkenness," or being "under the influence."

3.     For "Lend/Transfer/Sell" and "Produce," APIS collects only provisions that specifically refer to age 21 or that apply to the context of alcoholic beverage sales, and does not collect general prohibitions against producing, altering, lending, transferring, or selling licenses or identification cards.  In this case, "context" can include a provision's particular location in a statutory or regulatory code. For these variables, APIS does not include provisions that apply only to minors falsifying identification.

4.    Jurisdictions may require retailers to use reasonable care in asking for and inspecting the valid or proper identification provided by customers, where "valid identification" or "proper identification" are defined in terms of the official form of identification issued by the appropriate government agency.  In these situations (and in the absence of provisions establishing liability notwithstanding use of reasonable care), APIS interprets the reasonable care requirement as providing the retailer a defense for reasonable reliance on an apparently valid ID. 

5.   Provisions dealing with "youth" or "probationary" driver's licenses are not used to code the "Distinctive Licenses" variable unless these provisions otherwise require such licenses to be visually distinctive and distinguishable from licenses for persons 21 and over.

6.     Some States have provisions that make it a criminal offense to aid or abet a violation of the laws against underage purchase or possession, including a minor's use of false identification.  These provisions could be interpreted as prohibiting a person from producing, lending, transferring, or selling a false ID.  However, APIS does not code aiding and abetting provisions under "Lend/Transfer/Sell" or "Produce."

7.       For "Detention of Minor," APIS includes only provisions that are specific to detaining minors suspected of using false identification in the context of purchasing alcohol.  General police powers granted to liquor control employees in control States have not been examined by APIS (see ALCOHOLIC BEVERAGE CONTROL/Alcohol Control Systems).



   COLUMN DEFINITIONS (VARIABLES)
   ==============================

1.   Provisions That Target Minors  

-       Use of False ID Prohibited - A checkmark indicates that it is a criminal offense to use false identification to obtain alcohol.  

-       License Suspension - A State may impose an administrative license suspension (see Definitions) and/or a judicial suspension (see Definitions) for the use of false identification.  If there are separate judicial and administrative processes, and the administrative process is an independent process that can result in license suspension regardless of a conviction or suspension order by the court, this variable is coded as "both".  

(License suspension for underage purchase, possession or consumption of alcohol is addressed under the APIS policy topic, Use/Lose)
 
2.  Provisions That Target Suppliers 

-       Lend/Transfer/Sell - A checkmark indicates that it is a criminal offense to lend, transfer, or sell a false ID. For this variable, APIS does not include provisions that apply only to minors falsifying identification.

-      Produce - A checkmark indicates that it is a criminal offense to alter a valid ID or to create or manufacture a false ID. This variable requires that the law specifically refer to age 21 or apply to the context of alcoholic beverage sales. In this case, "context" can include a provision's particular location in a statutory or regulatory code. For this variable, APIS does not include provisions that apply only to minors falsifying identification.

3.  Retailer Support Provisions 

-       Scanner - A checkmark indicates that the State provides incentives to retailers who use electronic scanners that read birthdate and other information digitally encoded on valid identification cards.  Incentives may include an affirmative defense in prosecutions for sales to minors if the retailer can show that the scanner was used properly.

-       Distinctive Licenses - A checkmark indicates that driver's licenses for persons under 21 must be easily distinguishable from licenses for persons 21 and over.  This variable is not specific to alcoholic beverage sales.  

-       Seizure of an Identification Document- A checkmark indicates that retailers may seize apparently false IDs without fear of prosecution even if the identification is valid (assuming the retailer has a reasonable or good faith belief that the ID is false). 

-       Affirmative Defense - APIS codes two types of affirmative defenses that retailers can assert in license or criminal actions related to furnishing alcohol to a minor:

       .      Specific Affirmative Defense - The retailer inspected the false ID and came to a reasonable conclusion based on its appearance that it was valid.

       .    General Affirmative Defense - The retailer came to a good faith or reasonable decision that the purchaser was 21 years or older.  Inspection of an identification card is not required, although it may serve as evidence that the affirmative defense should be applied.

The code for this variable is "none" if the State has no statutory or regulatory provision that provides the retailer an affirmative defense related to his or her belief that the minor was 21 years of age or older.

The APIS policy topic, Furnishing Alcohol to Minors, provides another example of an affirmative defense.  In the case of Furnishing, a seller/licensee must be exonerated of charges of furnishing alcohol to a minor unless the minor involved is charged. 

-      Right to Sue Minor - A checkmark indicates that a retailer has the statutory right to sue a minor who uses a false ID to purchase alcohol for any losses or fines suffered by the retailer as a result of the illegal sale.

-      Detention of Minor - A checkmark indicates that a retailer has the authority to detain a minor suspected of using a false ID to purchase alcohol.  This authority may protect the retailer from liability for false arrest, false imprisonment, slander, or unlawful detention.



   JURISDICTION NOTES
   ==================

AR: In Arkansas, the prohibition against attempted use of a false ID for purchasing alcoholic beverages applies to persons less than 21 years of age.  Prior to July 31, 2007, the denial of driving privileges as a penalty for violating this prohibition only applied to persons less than 18 years of age. This denial is through a judicial process.  Beginning on July 31, 2007, Arkansas added an administrative suspension process for those between 18 to 21 years of age to whom the judicial process does not apply.

CO: In Colorado, the license revocation period for a first conviction of obtaining or attempting to obtain an alcoholic beverage by misrepresentation of age is twenty-four hours of public service, if ordered by the court, or three months.

DE: Although Del. Admin. Code § 2 2000 2215 states that "persons under 21 years of age have noted on their licenses 'Under 21,'" research revealed no Delaware statute or regulation expressly requiring distinguishing licenses for persons under 21 years of age.  This requirement is probably the result of an uncodified administrative decision not published in the Code of Delaware Regulations.  Because APIS research does not address administrative decisions or directives that are not included in a State's published regulatory codes, no check mark appears in the Distinguishing Licenses column for Delaware.

DC: Section designations in the District of Columbia Code were renumbered in connection with the publication of the D.C. Official Code, 2001 Edition.

FL: Although the verbatim text of historical regulations is only available from January 1, 2003 forward, the published historical information for Fla. Admin. Code Ann. r. 61A-3.052 indicates that this regulation has been in force, unchanged, from February 28, 1994 to date.

HI: In Hawaii, the retailer has a defense to a charge of furnishing to a minor if, in making the sale or allowing the consumption of liquor by a minor, the retailer was misled by the appearance of the minor and the attending circumstances into honestly believing that the minor was of legal age, and if the retailer can prove that he or she acted in good faith.

ID: Although the verbatim text of historical regulations is only available from January 1, 2002 forward, the published historical information for Idaho Admin. Code § 11.05.01.021 indicates that this regulation has been in force, unchanged, since March 31, 1995.

LA: In Louisiana, beginning January 1, 2000, and thereafter, special identification cards issued to applicants less than twenty-one years of age shall contain a highly visible distinctive color to clearly indicate that the card has been issued to an applicant less than twenty-one years of age.  Special identification cards are to be accepted as valid identification of the person to whom it was issued but does not enable the person to whom it is issued to operate a motor vehicle.   La. Rev. Stat. Ann. § 40:1321.

ME: In Maine, the Provisions Targeting Suppliers apply to acts prohibited  by minors.  The more general laws that address adults are not collected here as they are not, for APIS purposes, specific  to the lending, transfer, sale, or production of false identification for a minor's obtaining alcoholic beverages.

MD: In Maryland, a licensee or employee of the licensee may not be found guilty of underage furnishing if the person establishes to the satisfaction of the jury or the court sitting as a jury that the person used due caution to establish that the person under 21 years of age was not, in fact, a person under 21 years of age if a nonresident of the State.  This constitutes a general affirmative defense under APIS coding.  In contrast, if the person is a resident of the State of Maryland, the licensee or employee of the licensee may accept, as proof of a person's age, the person's driver's license or identification card as provided for in the Maryland Vehicle Law.  In addition, beginning October 1, 2006, the licensee or employee of the licensee may accept, as proof of a person's age, a United States military identification card.  These are examples of a specific affirmative defense under APIS coding.  See Md. Ann. Code, Art. 2B, § 12-108(a)(3)(ii)-(iii).

MI: Prior to July 1, 2003, Michigan's operator's licenses and official state personal identification cards issued to a person who at the time of application was 20-1/2 years of age or less, indicated that the cardholder was less than 21 years of age.

Although the authority of a retail licensee to confiscate an allegedly false identification is not explicit, the licensee shall present the alleged fraudulent identification to local law enforcement if it is in the possession of the licensee upon filing a police report concerning the violation.

See also Affirmative Defense: Minor Not Charged in the Furnishing Alcohol to Minors policy topic.

NH: In New Hampshire, the prohibition against the use of a false ID for purchasing alcoholic beverages applies to persons less than 21 years of age.  Before January 1, 2003, the denial of driving privileges as a penalty for violating this prohibition only applied to persons less than 18 years of age.  After January 1, 2003, the denial of driving privileges applies to those under 21 years of age.

SC: See also Affirmative Defense: Minor Not Charged in the Furnishing Alcohol to Minors policy topic.

TN: In Tennessee, APIS bases coding for the Specific Affirmative Defense afforded to retailers on Tenn. Code Ann. § 57-5-108. This statute provides that no permit or license shall be revoked on the grounds that the operator or any person working for the operator sells beer to a minor over the age of 18 years if such minor exhibits an identification, false or otherwise, indicating the minor's age to be 21 or over, if the minor's appearance as to maturity is such that the minor might reasonably be presumed to be of such age and is unknown to such person making the sale. As of July 1, 2006, it is also an affirmative defense to criminal prosecution if any person accused of giving or buying alcoholic beverages or beer for a minor acted upon a reasonably held belief that the minor was of legal age. Tenn. Code Ann. § 39-15-404.  APIS does not rely on this statute for coding of a General Affirmative Defense because § 57-5-108 applies more specifically to retailers.

VA: Virginia defines “bona fide evidence of legal age” as including any evidence that is or reasonably appears to be an unexpired driver's license issued by any state of the United States or the District of Columbia, military identification card, United States passport or foreign government visa, unexpired special identification card issued by the Department of Motor Vehicles, or any other valid government-issued identification card bearing the individual's photograph, signature, height, weight, and date of birth, or which bears a photograph that reasonably appears to match the appearance of the purchaser. A student identification card is not considered to be bona fide evidence of legal age.  See 3 Va. Admin. Code § 5-50-20 and Va. Code Ann. § 4.1-304(B).  In determining whether a licensee has reason to believe a purchaser is not of legal age, the Virginia Alcoholic Beverage Control Board considers whether an ordinary and prudent person would have reason to doubt that the purchaser is of legal age based on the general appearance, facial characteristics, behavior and manner of the purchaser, and whether the seller demanded, was shown and acted in good faith in reliance upon bona fide evidence of legal age that contained a photograph and physical description consistent with the appearance of the purchaser.  See 3 Va. Admin. Code § 5-50-20(A).  APIS has interpreted the “good faith reliance” requirement as providing the retailer a defense for reasonable reliance on an apparently valid ID.

 US: Please see Federal Law for this policy topic.




   ROW NOTES
   =========

AR: Under Ark. Code Ann. § 5-27-503(b), a seller's detention of a person under 21 for use of false identification "shall not include a physical detention."

DC: The District of Columbia defines a "valid identification document" as “an official identification issued by an agency of government (local, state, federal, or foreign) containing, at a minimum, the name, date of birth, signature, and photograph of the bearer." See D.C. Code Ann. § 25-101(53). D.C. Code Ann. § 25-783(b) requires licensed establishments to “take steps reasonably necessary to ascertain” whether any person to whom an alcoholic beverages is served is of legal drinking age, and further provides that “[a]ny person who supplies a valid identification document showing his or her age to be the legal drinking age shall be deemed to be of legal drinking age.”  APIS has interpreted the “reasonable steps” requirement as providing the retailer a defense for reasonable reliance on an apparently valid ID.

GA: In Georgia, the prohibition against furnishing to a minor does not apply when a retailer has been provided with “proper identification,” defined as “any document issued by a governmental agency containing a description of the person, such person's photograph, or both, and giving such person's date of birth.”  When a reasonable or prudent person could reasonably be in doubt as to whether a customer is of legal drinking age, the retailer has a duty to request to see and to be furnished with proper identification in order to verify the customer’s age, and the failure to make such request and verification in the case of an underage person may be considered by the trier of fact in determining whether the retailer furnishing the alcoholic beverage did so knowingly.  See Ga. Code Ann. § 3-3-23(d), (h).  APIS has interpreted the “reasonable or prudent person” requirement as providing the retailer a defense for reasonable reliance on an apparently valid ID.

ID: As of March 8, 2007, retailers are only required to deliver documents to law enforcement that have been lost or voluntarily surrendered; however, when presented with  identification documents that appear to be mutilated, altered, or fraudulent, they must contact law enforcement and refuse service.

MS: Although it appears the Mississippi Department of Public Safety currently still issues distinctive licenses for persons under 21, no codified statute or regulation requiring the issuance of such licenses has been found to exist after December 31, 2004.  APIS coding relies only on codified statutes and regulations and not on uncodified administrative decisions or directives, and therefore the check mark for Distinctive Licenses in Mississippi has been removed beginning on January 1, 2005.

OK: Beginning July 1, 2006, Oklahoma provides retailers a defense in criminal prosecutions for furnishing minors with "low-point beer" (defined as all beverages containing more than 0.5% alcohol by volume and not more than 3.2% alcohol by weight).  The defense takes the form of a rebuttable presumption that the retailer reasonably relied upon proof of age if (1) the minor presented what a reasonable person would have believed was a driver license or other government-issued photo identification purporting to establish that the individual was 21 years of age or older; or (2) the retailer confirmed the validity of the driver license or other government-issued photo identification presented by the individual by using a transaction scan device; and (3) if the retailer exercised reasonable diligence to determine whether the physical description and picture on the driver license or other
government-issued photo identification was that of the individual who
presented it.

VT: Vermont has two statutes regarding affirmative defenses.  First, under Vt. Stat. Ann. tit. 7, § 658, an employee of a licensee or of a state-contracted liquor agency charged with underage furnishing may plead as an affirmative defense that the employee carefully viewed specified photographic identification, that an ordinary prudent person would believe the purchaser to be of legal age to make the purchase, and that the sale was made in good faith, based upon the reasonable belief that the purchaser was of legal age to purchase alcoholic beverages.  APIS has interpreted the "good faith" and "reasonable belief" requirement as providing the employee a defense for reasonable reliance on an apparently valid ID.  Second, Vt. Stat. Ann. tit.7, § 602 provides that selling or furnishing to a person exhibiting "a valid authorized form of identification," which means a  valid photographic operator's license, enhanced driver's license, or valid photographic nondriver identification card issued by Vermont or another state or foreign jurisdiction, a United States military identification card, or a valid passport or passport card bearing the photograph and signature of the individual is prima facie evidence of the licensee's compliance with the law prohibiting the sale or furnishing of alcoholic beverages to minors.  The first provision amounts to a specific affirmative defense for state store employees and employees of retail licensees.  The second provision applies to licensees and appears to provide them at least limited protection from prosecution, although the statutory language is unclear regarding how the provision is to be applied.

