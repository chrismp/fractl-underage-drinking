
   CHANGES TO THE DATA
   ===================

Data for this policy topic are updated from time to time to add new material and to clarify or correct information already available on the site. A list of any changes since this policy topic first appeared on this Web site is available on the Web Site Change Log page.



   EXPLANATORY NOTES AND LIMITATIONS
   =================================

Explanatory Notes and Limitations Applicable to All APIS Policy Topics

1.   State law may permit local jurisdictions to impose requirements in addition to those mandated by State law. Alternatively, State law may prohibit local legislation on this topic, thereby preempting local powers. For more information on the preemption doctrine, see the About Alcohol Policy page. APIS does not document policies established by local governments.

2.   In addition to statutes and regulations, judicial decisions (case law) also may affect alcohol-related policies. APIS does not review case law except to determine whether judicial decisions have invalidated statutes or regulations that would otherwise affect the data presented in the comparison tables.

3.   APIS reviews published administrative regulations. However, administrative decisions or directives that are not included in a State's published regulatory codes may have an impact on implementation. This possibility has not been addressed by the APIS research.

4.   Statutes and regulations cited in tables on this policy topic may have been amended or repealed after the specific date or time period specified by the site user's search criteria.

5.   If a conflict exists between a statute and a regulation addressing the same legal issue, APIS coding relies on the statute.

6.   A comprehensive understanding of the data presented in the comparison tables for this policy topic requires examination of the applicable Row Notes and Jurisdiction Notes, which can be accessed from the body of the table via links in the Jurisdiction column.


Explanatory Notes and Limitations Specifically Applicable to Loss of Driving Privileges for Alcohol Violations by Minors 

1.   APIS only includes provisions related to first offenses.  States that impose a license sanction for repeat offenses only are not coded for this policy. 

2.   Because of the high level of discretion in the juvenile courts, a judge might impose a license sanction even in a State where there is no Use/Lose law. 

3.   Some States specify license denial (loss of eligibility to obtain the driver's license for a specified period of time) as a sanction for an underage alcohol offense by a minor who does not already have a driver's license.  APIS does not address denial sanctions. 

4.   This review does not address the following issues related to loss of driving privileges for alcohol violations by minors:

        ·    Provisions related only to non-alcohol violations, including illicit drug violations, or to commercial driver's licenses. 

        ·    Provisions that are not specific to persons under 21 years of age.

        ·    Provisions that apply only in specified settings, e.g., underage possession or consumption on school property or in a motor vehicle, or underage consumption or presence on licensed premises.

        ·    Provisions that allow for a license suspension or revocation to be reviewed and driving privileges restored, that provide for "restricted," "limited," or "hardship" licenses, under which individuals subject to license suspension or revocation may be permitted to drive for purposes of employment or medical treatment, or that address diversion, plea bargaining, conditions of probation, or discretion for judges to reduce a suspension or revocation period.

        ·    Provisions authorizing a law enforcement officer to retain the driver's license of a person arrested for underage alcohol purchase, possession or consumption, in order to ensure the person's later appearance to answer the charges against him or her.

        ·    Provisions that impose a license sanction for failure of or refusal to submit to a chemical test.

       ·     Provisions that are limited to prohibiting underage intoxication, as opposed to underage consumption per se. 

5.   A number of States have passed laws prohibiting the "internal possession" of alcohol by underage persons.  These provisions typically require evidence of alcohol in the minor's body, but do not require any specific evidence of possession or consumption.  APIS does not code violations of either Possession or Consumption laws as a trigger for license action solely on the basis of an "internal possession" provision. 

6.    Some States prohibit minors from purchasing alcohol only when the minor makes a false statement of his/her age or presents a false identification (see the APIS policy topic, False Identification for Obtaining Alcohol). States with these limited prohibitions are not coded as having a use/lose provision for purchase.



   COLUMN DEFINITIONS (VARIABLES)
   ==============================

1.   Type of Violation Leading to License Suspension or Revocation

APIS codes three types of violation for which a young person's license may be suspended or revoked (see Definitions):

.   Purchase of Alcohol
.   Possession of Alcohol  
.   Consumption of Alcohol
 
Checkmarks are shown in the Purchase and Possession columns if the relevant State provisions include actual or attempted purchase and possession.

Note: Separate APIS policy topics provide details of States' provisions pertaining to Underage Purchase, Possession, and Consumption of alcohol.


2.    Upper Age Limit 
         
The age below which the license suspension/revocation sanction applies.
 
 
3.   Authority to Impose License Sanction

This column indicates whether the State's authority to impose driver's license sanctions for underage alcohol violations is mandatory or discretionary. 


4.    Length of Suspension/Revocation in Days 

The column displays the minimum and maximum number of days of suspension or revocation specified in statutes or regulations.  If the penalty is discretionary and a minimum term is not specified, then "0" days has been entered in the "Minimum" column to indicate that the court has the discretion not to impose any period of suspension or revocation.  Some States make the penalty discretionary but specify a period of time for the suspension or revocation should the court exercise this discretion.  In these cases, the minimum and maximum period is included in these columns.  When the statute or regulation specifies "months," APIS uses a 30-day conversion to determine number of days per month.  The abbreviation "n/s" indicates that the minimum or maximum figure for a mandatory sanction is not specified in the relevant statute or regulation.



   JURISDICTION NOTES
   ==================

CO: In Colorado, the license revocation period for a first conviction of underage purchase, possession, or consumption is twenty-four hours of public service, if ordered by the court, or three months.

DC: Section designations in the District of Columbia Code were renumbered in connection with the publication of the D.C. Official Code, 2001 Edition.

GA: With respect to underage possession, Georgia imposes a license sanction only if the possession occurs while operating a motor vehicle.  See Ga. Code Ann. § 40-5-63(e).  APIS does not address provisions that apply only when the minor is the operator or passenger of a motor vehicle.

LA: In Louisiana, a person between the ages of 13 and 18 years old who is adjudicated delinquent of a crime or offense involving alcohol shall lose his or her driving privileges for not less than 90 days but not more than one year or until he or she has reached the age of 18 years, whichever is longer.

OR: Whenever a person who is 17 years of age or younger, but not younger than 13 years of age, is convicted of any offense involving the use or abuse of alcohol, the Department of Transportation shall impose a suspension for one year, or until the person so suspended reaches 17 years of age, whichever is longer.

TN: In Tennessee, the driving privileges of an offender shall be suspended for one year, or until the offender reaches 17 years of age, whichever is longer.

VT: In Vermont, suspension of a person’s driver’s license for underage possession or consumption is only imposed upon the person's failure to successfully complete a diversion program.  APIS does not address diversion provisions.

WA: In Washington, the driving privileges of a juvenile shall be revoked for one year, or until the juvenile reaches 17 years of age, whichever is longer.

 US: Please see Federal Law for this policy topic.




   ROW NOTES
   =========

CT: In addition to the 30 day suspension penalty mentioned in the table above, Connecticut imposes a license suspension of 60 days if underage possession occurs "on any public street or highway."  See Conn. Gen. Stat. §§ 14-111e(a), 30-89(b)(1).  APIS does not code provisions that apply only when the minor is located on a public street or highway.

MO: Although Missouri does not authorize a Use / Lose penalty for all underage consumption, a law that became effective on August 28, 2005 imposes the mandatory license sanction on an underage person who “has a detectable blood alcohol content of more than two-hundredths of one percent or more by weight of alcohol in such person's blood.” See Mo. Rev. Stat. §§ 311.325(1), 577.500(2).

NH: Although New Hampshire does not authorize a Use / Lose penalty for all underage consumption, a law that became effective on January 1, 2003, imposes a discretionary license sanction on minors who are "intoxicated by consumption of an alcoholic beverage," and provides that an alcohol concentration "of .02 or more shall be prima facie evidence of intoxication.” See N.H. Rev. Stat. Ann. §§ 179:10(I), 263:56-b.

OK: In Oklahoma, the denial of driving privileges is a consequence imposed on those under 18 years who have possessed an intoxicating beverage or purchased, possessed, or consumed low-point beer (defined as containing not more than 3.2 percent ABW).  Between July 1, 2006 and October 31, 2010, the law required the court to order the Department of Public Safety to cancel or deny driving privileges for a period of 6 months for a first offense from the date of the offense or from the date the person reaches 16 years, whichever period of time is longer.  In addition, the court has the discretion to impose a longer cancellation or denial period which can extend to the offender’s 21st birthday.  After November 1, 2010, the legislature did not change the 6 month mandatory period and the provision regarding the court’s discretion to impose a longer penalty but revised the law’s application to those under 16 years of age.  It no longer requires the court to begin the 6 month mandatory cancellation or denial period upon the 16th birthday.  It instead requires that the period be extended to the offender’s 16th birthday if the period would otherwise end before that date.

