#!/bin/bash

for i in ./*/
do
	cd "$i";
	echo "#dbg:i=$i"
	unzip *.zip;
	cd ../;
done
