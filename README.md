# Underage drinking laws and underage drinking

This project compares strictness of states' underage drinking laws with how often their minors drink illegally.

Datasets for drinking laws are in `_raw/`.

# How to read this (example)
1. Parameter Name aka CODE_HERE
..* Column name
...* Name of Value 				+0 
...* Value, or another value 	+1
..* Column name, another column name [NOTE: THIS MEANS THE FOLLOWING SCORING SYSTEM CORRESPONDS TO BOTH THESE COLUMN NAMES]
...* (blank) +0 [NOTE: THIS MEANS IF VALUE HERE IS BLANK, ADD NOTHING TO THE PARAMETER'S SCORE]


I add scores from each relevant column into a column named something like PARAMETER_NAME_TOTAL


# Calculating state law's strictness

1. Driving aka UL
..* Purchase, Possession, Consumption
...* Unchecked, (blank)	+0
...* Checked			+1
..* Authority to Impose License Sanction
...* (blank)		+0
...* discretionary	+1
...* mandatory		+2
..* Length of Suspension Minimum (IMPORTANT NOTE: I DO NOT INCLUDE THIS IN MY FINAL CALCULATION OF STRICTNESS SCORE BECAUSE IT SKEWS THE STRICTNESS SCORES UNFAIRLY; MINIMUM JAIL TIME OR FINES ARE NOT PRESENT IN OTHER SCORING PARAMETERS)
...* n/s +0 [SINCE THIS COLUMN IS EXCLUDED, DO NOT ADD THIS]
...* +(value) [SINCE THIS COLUMN IS EXCLUDED, DO NOT ADD THIS]
AFTER CACULATING EACH ROW, GROUP THEM BY STATE, AVERAGE THE TOTAL SCORES

2. Fake ID aka Fi
..* Use of False ID Prohibited, Lend/Transfer/Sell, Produce, Scanner, Distinctive Licenses, Seizure of ID, Right to Sue Minor, Detention of Minor
...* Unchecked, (blank)	+0
...* Checked			+1

3. Furnishing aka UAFR
..* Furnishing Prohibited
...* Unchecked, (blank)	+0
...* Checked			+1
..* Exception for: Parent/Guardian, Spouse, In any private location, In any private residence, In parent/guardian home only
...* (blank)	+0
...* 1			-2
...* 2			-1

4. Possession and Consumption AKA UACB
..* Conduct is Prohibited
...* (blank), "-"	+0
...* Yes			+1
..* Exception for: Parent/Guardian, Spouse, In any private location, In any private residence, In parent/guardian home only
...* (blank)	+0
...* 1			-2
...* 2			-1

5. Purchasing aka UAPR
..* Purhcase Prohibited
...* Unchecked, (blank)	+0
...* Checked			+1
..*  Youth May Purchase for Law Enforcement Purposes
...* Unchecked, (blank) +1
...* Checked						+0

6. Selling aka UAOF
..* Minimum Age To Sell:  Beer, Minimum Age To Sell:  Wine, Minimum Age To Sell:  Spirits
...* +(value)
..*  Manager or Supervisor Must Be Present
...* Unchecked, (blank)	+0
...* Checked			+1

7. Serving aka UAON
..* Minimum Age To Serve:  Beer, Minimum Age To Serve:  Wine, Minimum Age To Serve:  Spirits, Minimum Age To Bartend:  Beer, Minimum Age To Bartend:  Wine, Minimum Age To Bartend:  Spirits
...* +(value)
..* Manager or Supervisor Must Be Present
...* Unchecked, (blank)	+0
...* Checked			+1